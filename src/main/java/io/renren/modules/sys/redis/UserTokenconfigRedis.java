package io.renren.modules.sys.redis;

import io.renren.common.utils.RedisKeys;
import io.renren.common.utils.RedisUtils;
import io.renren.modules.sys.entity.SysUserTokenEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component

public class UserTokenconfigRedis {

    /**
     * 将系统配置放在Redis缓存里
     *
     * @author chenshun
     * @email sunlightcs@gmail.com
     * @date 2017/7/18 21:08
     */


        @Autowired
        private RedisUtils redisUtils;


        public void saveOrUpdate(SysUserTokenEntity token) {
            if(token == null){
                return ;
            }
            String key = RedisKeys.getUerTokenkey(token.getToken());
            redisUtils.set(key, token);
        }

        public void delete(String token) {
            redisUtils.delete(token);
        }

        public SysUserTokenEntity get(String token){
            String key = RedisKeys.getUerTokenkey(token);
            return redisUtils.get(key, SysUserTokenEntity.class);
        }
    }


